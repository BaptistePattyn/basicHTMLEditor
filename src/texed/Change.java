/**
 * Implementation of a change that can be done to a text document.
 * Contains the textpart and the action, being add or del
 */

package texed;

public class Change {
	private TextPart text;
	private String action;
	/*
	 * Constructor for a change 
	 * @param text: the textpart to add
	 * @param action: the action that has been performed
	 */
	public Change(TextPart text, String action) {
		this.text = text;
		this.action = action;
	}
	/*
	 * @return the textpart
	 * 	 */
	public TextPart getTextPart() {
		return text;
	}
	/*
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
}
