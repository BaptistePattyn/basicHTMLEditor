package texed;

/**
 * Defines a stack of elements 
 *
 * @param <E> type of the element
 */
public interface Stack<E> {
	/**
	 * Pushes an element on the stack
	 * 
	 * @param element
	 */
	public void push(E element);
	
	/**
	 * 
	 * @return the element at the top of the stack
	 */
	public E top();
	
	/**
	 * Pops the element at the top of the stack
	 * 
	 * @return the element popped
	 */
	public E pop();
	
	/**
	 * 
	 * @return the number of elements on the stack
	 */
	public int size();
	
	/**
	 * 
	 * @return true if the stack contains no elements, otherwise false 
	 */
	public boolean isEmpty();
}
