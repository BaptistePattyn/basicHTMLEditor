/**
 * 
 */
package texed;

import java.util.EmptyStackException;
import java.util.Iterator;

/**
 * @author Piet
 *
 */
public class StackAdapter<E> implements Stack<E>, Iterable<E> {

	private LinkedList<E> list;
	
	public StackAdapter() {
		list = new LinkedList<E>();
	}
	
	public StackAdapter(LinkedList<E> list){
		this.list = list;
	}
	
	/* (non-Javadoc)
	 * @see algoritmen.Stack#push(java.lang.Object)
	 */
	@Override
	public void push(E element) {
		list.prepend(element);
	}

	/* (non-Javadoc)
	 * @see algoritmen.Stack#top()
	 */
	@Override
	public E top() {
		if(isEmpty()) throw new EmptyStackException();
		return list.getHead();
	}

	/* (non-Javadoc)
	 * @see algoritmen.Stack#pop()
	 */
	@Override
	public E pop() {
		E element = top();
		list = list.getTail();
		return element;
	}

	/* (non-Javadoc)
	 * @see algoritmen.Stack#size()
	 */
	@Override
	public int size() {
		return list.getSize();
	}

	/* (non-Javadoc)
	 * @see algoritmen.Stack#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public Iterator<E> iterator() {
		return new StackAdapterIterator();
	}
	
	public LinkedList<E> getList(){
		return this.list;
	}
	
	public StackAdapter<E> invert(){
		StackAdapter<E> returnStack = new StackAdapter<>();
		for(E e : this) {
			returnStack.push(e);
		}
		return returnStack;
	}
	
	/*public Boolean equals(StackAdapter<E> stack) {
		boolean returnBool = true;
		System.out.println(this.getList().getSize() +", " + stack.getList().getSize());
		if(this.getList().getSize() != stack.getList().getSize()) {
			returnBool = false;
			System.out.println("Not the same size");
		} else {
			while(!(this.isEmpty() || stack.isEmpty())) {
				if(!this.pop().equals(stack.pop())) {
					System.out.println("Not the same elements");
					returnBool = false;
				}
			}
		}
		return returnBool;
		
	}*/
	
	private class StackAdapterIterator implements Iterator<E>{
		
		@Override
		public boolean hasNext() {
			return !isEmpty();
		}

		@Override
		
		public E next() {
			E element = pop();
			return element;
		}
		
	}

	
}
