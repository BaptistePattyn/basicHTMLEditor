/**
 * Basic text editor that provides support for automatically closing opened HTML-tags.
 */

package texed;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneLayout;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

/**
 * Simple GUI for a text editor.
 *
 */
public class Texed extends JFrame implements DocumentListener, ActionListener {
	private JTextArea textArea;
	protected JButton undo, redo, check, reset;
	private JPanel mainPane = new JPanel();
	private int cursorPos = 0;
	private StackAdapter<Change> undoStack = new StackAdapter<>();
	private StackAdapter<Change> redoStack = new StackAdapter<>();
	private String character;
	private boolean buttonBool = false;
	private boolean tagBool = false;
	private String previousText = "";


	private static final long serialVersionUID = 5514566716849599754L;
	/**
	 * Constructs a new GUI: A TextArea on a ScrollPane
	 */
	public Texed() {
		
		
		
		super();
		
		mainPane.setSize(800,600);
		
		JPanel buttonPane = new JPanel();
		undo = new JButton("Undo");
		undo.addActionListener(this);
		redo = new JButton("Redo");
		redo.addActionListener(this);
		check = new JButton("Check");
		check.addActionListener(this);
		reset = new JButton("Reset");
		reset.addActionListener(this);
				
		setTitle("Texed: simple text editor");
		setBounds(800, 800, 600, 600);
		textArea = new JTextArea(30, 80);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		
		//Registration of the callback
		textArea.getDocument().addDocumentListener(this);
		
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setLayout(new ScrollPaneLayout());
		
		buttonPane.add(undo);
		buttonPane.add(redo);
		buttonPane.add(check);
		buttonPane.add(reset);
		
		mainPane.add(buttonPane);
		mainPane.add(scrollPane);
		
		add(mainPane);
		pack();
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);		
	}

	/**
	 * Callback when changing an element
	 */
	public void changedUpdate(DocumentEvent ev) {
		cursorPos = textArea.getCaretPosition();
	}

	/**
	 * Callback when deleting an element
	 * Only call this method when it is not activated by the buttons
	 * The removed string is being stored in the undoStack with the removed text, the position and the action Del
	 */
	public void removeUpdate(DocumentEvent ev) {
		if(!buttonBool && previousText != null) {
			redoStack = new StackAdapter<>();
			String removedStr = previousText.substring(ev.getOffset(), ev.getOffset()+ev.getLength());
			cursorPos = textArea.getCaretPosition();
			undoStack.push(new Change(new TextPart(removedStr,cursorPos-1),"Del"));
				}
			} 		
	/**
	 * Callback when inserting an element
	 * only do this method when it is not activated by the buttons
	 * The added string is being stored in the undoStack with the added text, the position and the action Add
	 */
	public void insertUpdate(DocumentEvent ev) {
		if(!buttonBool) {
			redoStack = new StackAdapter<Change>();
			cursorPos = textArea.getCaretPosition();
			if(ev.getLength() != 1) return;
			try {
				character = textArea.getText(ev.getOffset(), ev.getLength());
				if(character.equals(">")) {
					addClosingTag(ev);
				}
				undoStack.push(new Change(new TextPart(character, cursorPos),"Add"));
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			SwingUtilities.invokeLater(new Task("", cursorPos));
		}
		
	}

	/**
	 * Runnable: change UI elements as a result of a callback
	 * Start a new Task by invoking it through SwingUtilities.invokeLater
	 */
	private class Task implements Runnable {
		private String text;
		private int pos;
		/**
		 * Pass parameters in the Runnable constructor to pass data from the callback 
		 * @param text which will be appended with every character
		 */
		Task(String text, int pos) {
			this.text = text;
			this.pos = pos;
		}

		/**
		 * The entry point of the runnable
		 */
		public void run() {
			textArea.insert(text, pos);
			if(tagBool) {
				undoStack.push(new Change(new TextPart(text,  pos), "Add"));
				textArea.setCaretPosition(pos);
			}
			previousText = textArea.getText();
			tagBool = false;
		}
	}

	/**
	 * Entry point of the application: starts a GUI
	 */
	public static void main(String[] args) {
		new Texed();

	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		previousText = textArea.getText();
		/*
		 * method to call when the undo button is pushed
		 * the first element of the undoStack gets popped and is used to perform the correct action
		 * First we check if the action is Add or Del then we execute the code to put the text back or to delete it
		 * The textpart gets pushed to the redoStack
		 */
		if(e.getActionCommand().equals("Undo")) {
			if(!undoStack.isEmpty()) {
				buttonBool = true;
				Change toDo = undoStack.pop();
				if(toDo.getAction().equals("Del")) {
					textArea.insert(toDo.getTextPart().getText(), toDo.getTextPart().getPosition());
				} else {
					StringBuffer sb = new StringBuffer(textArea.getText());
					if(sb.length() != 0){
						sb.delete(toDo.getTextPart().getPosition(), toDo.getTextPart().getPosition() + toDo.getTextPart().getText().length());
					}			
					textArea.setText(sb.toString());
				}
				redoStack.push(toDo);
				buttonBool = false;
			}
			/*
			 * method that executes the first element from the redoStack and puts it back on the undoStack
			 * works in the same way the undo method does
			 */
		} else if(e.getActionCommand().equals("Redo")) {
			if(!redoStack.isEmpty()) {
				buttonBool = true;
				Change toDo = redoStack.pop();
				if(toDo.getAction().equals("Del")) {
					StringBuffer sb = new StringBuffer(textArea.getText());
					if(sb.length() != 0){
						sb.delete(toDo.getTextPart().getPosition(), toDo.getTextPart().getPosition() + toDo.getTextPart().getText().length());
					}			
					textArea.setText(sb.toString());
				} else {
					textArea.insert(toDo.getTextPart().getText(), toDo.getTextPart().getPosition());
				}
				undoStack.push(toDo);
				buttonBool = false;
			}
			
		} else if(e.getActionCommand().equals("Check")) {
			if(checkTags()) {
				System.out.println("Tags match");
			}
			/*
			 * the reset button allows you to clear the textarea and reset all the stacks
			 */
		} else if(e.getActionCommand().equals("Reset")) {
			textArea.setText("");
			undoStack = new StackAdapter<Change>();
			redoStack = new StackAdapter<Change>();
		}
	}
	/*
	 * Method to print a stack that consists of change elements
	 * was used in debugging
	 */
	private void printStack(StackAdapter<Change> stack) {
		
		LinkedList<Change> list = stack.getList();
		for(Change change : list) {
			System.out.println(change.getTextPart().getText() + ", " + change.getTextPart().getPosition()+", " + change.getAction());
		}
		System.out.println("---------------------------------");
	}
	/*
	 * method that adds a closing tag when an opening tag is closed with the >-bracket
	 */
	private void addClosingTag(DocumentEvent ev) {
		
		int i = 0;
		try {
			while(!(textArea.getText(cursorPos-i, 1).equals("<"))) {
				i++;
			}
		} catch (BadLocationException e1) {
			
			e1.printStackTrace();
		} try {
			if(textArea.getText(cursorPos-i+1,1).equals("/")) {
				return;
			}
		} catch (BadLocationException e1) {
			
			e1.printStackTrace();
		}
		if(checkTags()) {
			return;
		} else {
		try {
			tagBool = true;
			i = 0;
			while(!(textArea.getText(cursorPos-i, 1).equals("<"))) {
				i++;
			} 
			int tagPos = cursorPos - i;
			String text = textArea.getText(tagPos +1, cursorPos - tagPos -1);
			SwingUtilities.invokeLater(new Task("</"+text+">", cursorPos+1));
		} catch (BadLocationException e) {
			e.printStackTrace();
			//System.out.println("Start of tag not found");
		}
		}
	}
	/*
	 * method that checks if all of the opened tags are closed
	 */
	private boolean checkTags() {
		previousText = textArea.getText();
		String[] splitted = previousText.split("");
		int tagOpening = 0;
		int tagClosing = 0;
		StackAdapter<String> open = new StackAdapter<>();
		StackAdapter<String> closing = new StackAdapter<>();
		for(int a = 0; a < splitted.length; a++) {
			if(splitted[a].equals("<") && !(splitted[a+1].equals("/"))) {
				tagOpening = a;
				int c = a;
				while(!(splitted[c].equals(">")) && c < splitted.length-1) {
					c++;
				}  	tagClosing = c;
				StringBuffer sb2 = new StringBuffer();
				for(int b = tagOpening+1; b < tagClosing; b++) {
					sb2.append(splitted[b]);
				}
				open.push(sb2.toString());
			} else if(splitted[a].equals("<") && (splitted[a+1].equals("/"))) {
				tagOpening = a;
				int c = a;
				while(!(splitted[c].equals(">")) && c < splitted.length -1) {
					c++;
				} tagClosing = c ;
				StringBuffer sb2 = new StringBuffer();
				for(int b = tagOpening+2; b < tagClosing; b++) {
					sb2.append(splitted[b]);
				}
				if(open.top().equals(sb2.toString())) {
					open.pop();
				}
				closing.push(sb2.toString());
			}
		}
		return open.isEmpty();
	}
}