/**
 * class that contains a textpart. Each textpart contains of a string, the text and an int being the position
 */

package texed;

public class TextPart {
	private int position;
	private String text;
	
	/*
	 * constructor for the textpart with text and a position
	 * @param text is the text from the textpart
	 * @param position: the position of the textpart in the text
	 */
	public TextPart(String text, int position) {
		this.text = text;
		this.position = position;
	}
	
	/*
	 * setter for the position
	 * @param pos: the position to set for the textpart
	 */
	public void setPosition(int pos) {
		this.position = pos;
	}
	/*
	 * @return the position of the textpart in the text
	 */
	public int getPosition() {
		return this.position;
	}
	
	/*
	 * @return the text from the textpart
	 */
	public String getText() {
		return this.text;
	}
}
